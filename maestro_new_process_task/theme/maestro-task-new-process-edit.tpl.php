<?php

/**
 * @file
 * maestro-task-interactive-function-edit.tpl.php
 */

?>

<table>
  <tr>
    <td><?php print t('Template:'); ?></td>
    <td><?php
    $select = array(
      'element' => array(
        '#attributes' => array(
          'name' => 'template',
        ),
        '#options' => $td_rec->templates,
        '#value' => isset($td_rec->task_data['template']) ? $td_rec->task_data['template'] : FALSE,
      ),
    );
    print theme('select', $select);
    ?></td>
  </tr>
  <tr>
    <td colspan="2"><?php print t('Related:'); ?></td>
  </tr>
  <tr>
    <td colspan="2"><?php
    $select = array(
      'element' => array(
        '#attributes' => array(
          'name' => 'related',
        ),
        '#title' => '',
        '#value' => isset($td_rec->task_data['related']) ? $td_rec->task_data['related'] : FALSE,
        '#return_value' => TRUE,
      ),
    );
    print theme('checkbox', $select);
    ?></td>
  </tr>
  <tr>
    <td colspan="2" style="font-style: italic; font-size: 0.8em;"><?php print t('A new process of the given template will be launched. If Related is checked, the process will be associated to the current project.'); ?></td>
  </tr>
</table>
