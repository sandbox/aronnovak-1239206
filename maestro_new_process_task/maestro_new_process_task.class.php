<?php

/**
 * Maestro Admin Interface Class
 */

class MaestroTaskInterfaceNewProcess extends MaestroTaskInterface {

  function __construct($task_id = 0, $template_id = 0) {
    $this->_task_type = 'NewProcess';
    $this->_is_interactive = MaestroInteractiveFlag::IS_NOT_INTERACTIVE;

    parent::__construct($task_id, $template_id);

    $this->_task_edit_tabs = array();
  }

  function display() {
    return theme('maestro_task_new_process', array('tdid' => $this->_task_id, 'taskname' => $this->_taskname, 'ti' => $this));
  }

  function getEditFormContent() {
    $this->_fetchTaskInformation();
    $res = db_query("SELECT id, template_name FROM {maestro_template}");
    $options = array();
    foreach ($res as $rec) {
      $options[$rec->id] = $rec->template_name;
    }
    $this->_task_data->templates = $options;
    return theme('maestro_task_new_process_edit', array('tdid' => $this->_task_id, 'td_rec' => $this->_task_data));
  }

  function save() {
    $rec = new stdClass();
    $rec->id = $_POST['template_data_id'];
    $rec->task_data = serialize(array('template' => $_POST['template'], 'related' => $_POST['related']));

    drupal_write_record('maestro_template_data', $rec, array('id'));

    return parent::save();
  }
}

class MaestroTaskTypeNewProcess extends MaestroTask {
  function execute() {
    $serializedData = db_query("SELECT task_data FROM {maestro_template_data} WHERE id = :tid",
    array(':tid' => $this->_properties->template_data_id))->fetchField();
    $taskdata = @unserialize($serializedData);
    $taskdata['current_pid'] = $this->_properties->process_id;
    maestro_new_process_task_exit($taskdata);

    $this->setRunOnceFlag($this->_properties->id);
    $this->completionStatus = TRUE;
    $this->executionStatus = TRUE;
    return $this;
  }

  function prepareTask() {}
}